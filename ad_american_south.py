from pdf_fixer import PdfFixer

fixer = PdfFixer(
    'AckermanAndDuvalAmericanSouth.pdf',
    'ADSouth_p306.pdf',
)

fixer.take(0, end=2)
fixer.take(0, doc=1)
fixer.take(3, end=())

fixer.rotate(180, lambda indx: indx % 2 == 0)

fixer.fix('AckermanAndDuvalAmericanSouthFixed.pdf')

"""
from PyPDF2 import PdfReader, PdfWriter

reader = PdfReader('AckermanAndDuvalAmericanSouth.pdf')
p306 = PdfReader('ADSouth_p306.pdf')
writer = PdfWriter()

for (indx, page) in enumerate(reader.pages):
    if indx == 2:
        writer.add_page(p306.pages[0].rotate(180))
    elif indx % 2 == 0:
        writer.add_page(page.rotate(180))
    else:
        writer.add_page(page)

with open('AckermanAndDuvalAmericanSouthFixed.pdf', "wb") as outfile:
    writer.write(outfile)
"""
