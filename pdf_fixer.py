from PyPDF2 import PdfReader, PdfWriter

class Rotate:
    def __init__(self, amount, where):
        self.amount = amount
        self.where = where
    
    def apply(self, page, indx):
        should_rotate = type(self.where) is tuple \
            or (type(self.where) is int and indx == self.where) \
            or (type(self.where) is list and indx in self.where) \
            or (callable(self.where) and self.where(indx))
        
        if should_rotate:
            print(f"\tpage rotated {self.amount}")
            return page.rotate(self.amount)
        else:
            return page

class PdfFixer:

    def __init__(self, *files):
        # Readers by indx
        self.docs = list(map(PdfReader, files))
        # elem => (doc_indx, transform)
        self.transforms = []
        
        # elem => (doc_indx, pages_range)
        self.takes = []
    
    """
    # Rotate operates on the output pages
    
    # Rotate pages 2 and 3 by 180 degrees
    fixer.rotate(180, [2, 3])
    
    # Rotate every even-indexed page by 90 degrees
    fixer.rotate(90, lambda indx: indx % 2 == 0)
    """
    def rotate(self, amount, where):
        self.transforms.append(Rotate(amount, where))
    
    """
    # Take only the first page
    fixer.take(0)
    
    # Take all the pages 0 thru 2
    fixer.take(0, end=3, doc=1)
    
    # `take()`s are stored in order, so doing this:
    fixer.take(1)
    fixer.take(0)
    # will reverse the order of the pages
    
    # Pass a random type to end in order to take up to the end
    fixer.take(0, end=(), doc=1)
    """
    def take(self, start, end="bad_default", doc=0):
        if type(start) is not int:
            raise ValueError('start was not an int')
        if type(end) is str: # hacks yay
            end = start + 1
        
        if type(end) is int:
            self.takes.append((doc, range(start, end)))
        else:
            doclen = len(self.docs[doc].pages)
            self.takes.append((doc, range(start, doclen)))

    def fix(self, outfile: str):
        if len(self.takes) == 0:
            self.take(0, end=())
        
        #print(self.takes)
        
        writer = PdfWriter()
        
        output_indx = 0
        for (doc_indx, rng) in self.takes:
            for indx in rng:
                page = self.docs[doc_indx].pages[indx]
                
                print(f"{output_indx}: reading page {indx} of doc {doc_indx}")
                
                for trans in self.transforms:
                    page = trans.apply(page, output_indx)
                
                writer.add_page(page)
                output_indx += 1
        
        with open(outfile, "wb") as of:
            writer.write(of)
