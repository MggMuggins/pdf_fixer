from pdf_fixer import PdfFixer

fixer = PdfFixer('8Stages.pdf')

fixer.rotate(180, [1, 3, 6, 8, 10, 13])
fixer.rotate(270, 11)

fixer.fix('8StagesUpsideDown.pdf')

fixer = PdfFixer('8StagesUpsideDown.pdf')

fixer.rotate(180, ())
fixer.fix('8StagesFixed.pdf')

"""
# To be clear, this code doesn't work
from PyPDF2 import PdfReader, PdfWriter

reader = PdfReader('8Stages.pdf')
writer = PdfWriter()

for (indx, page) in enumerate(reader.pages):
    #if indx == 2:
    #    writer.add_page(p306.pages[0].rotate(180))
    should_rotate = (indx % 2 == 1 and indx < 4) \
        or (indx % 2 == 0 and indx > 4 and indx < 11) \
        or indx == 13
    
    if should_rotate:
        writer.add_page(page.rotate(180))
    else:
        writer.add_page(page)

with open('8StagesFixed.pdf', "wb") as outfile:
    writer.write(outfile)
"""
