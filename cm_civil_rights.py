from pdf_fixer import PdfFixer

fixer = PdfFixer(
    "CM_CivilRightsMovement_1.pdf",
    "CM_CivilRightsMovement_2.pdf"
)

fixer.take(0, end=2, doc=0)
fixer.take(3, doc=0)

fixer.take(0, end=(), doc=1)

fixer.fix('CM_CivilRightsMovement.pdf')

"""
from PyPDF2 import PdfReader, PdfWriter

title = PdfReader("CM_CivilRightsMovement_1.pdf")
rest = PdfReader("CM_CivilRightsMovement_2.pdf")
writer = PdfWriter()

for indx in range(0, 2):
    writer.add_page(title.pages[indx])
    
writer.add_page(title.pages[3])

for page in rest.pages:
    writer.add_page(page)

with open("CM_CivilRightsMovement.pdf", "wb") as outfile:
    writer.write(outfile)
"""
