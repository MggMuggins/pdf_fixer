# PdfFixer
Provides a basic class that allows quick fixing up of pdfs where rotations
and page interleaving is not correct.

Use `pdf_fixer.py` with your scripts, see the code for documentation and the
other py files for examples. I didn't include the pdfs for the examples to
avoid dealing with copyright.

```py
from pdf_fixer import PdfFixer

fixer = PdfFixer(
  'Pdf0.pdf',
  'Pdf1.pdf',
)

# First two pages of Pdf1.pdf
fixer.take(0, end=2, doc=1)
# Rest of Pdf0.pdf
fixer.take(2, end=())

# Rotations affect output pages
fixer.rotate(180, [0, 1])

fixer.fix('Pdf.pdf')
```
